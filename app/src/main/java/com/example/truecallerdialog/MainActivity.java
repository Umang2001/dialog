package com.example.truecallerdialog;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private LinearLayout linearLayout;
    float xDown=0,yDown=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void opendialog(View view) {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);

        View mview= getLayoutInflater().inflate(R.layout.custom_dialog,null);

        dialog.setView(mview);
        final AlertDialog alertDialog = dialog.create();

        alertDialog.show();

//        mview.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View view, MotionEvent motionEvent) {
//
//                double movedX = motionEvent.getX();
//                double movedY = motionEvent.getY();
//
//                WindowManager.LayoutParams layoutParams = alertDialog.getWindow().getAttributes();
//                layoutParams.x = (int)movedX;
//                layoutParams.y = (int)movedY;
//                alertDialog.getWindow().setAttributes(layoutParams);
//
//                return true;
//            }
//        });

    }

    public void openSpamDialog(View view) {

        final AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);

        View mview= getLayoutInflater().inflate(R.layout.spam_custom_dialog,null);

        dialog.setView(mview);
        final AlertDialog alertDialog = dialog.create();

        alertDialog.show();


    }
}